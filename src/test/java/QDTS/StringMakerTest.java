package QDTS;

import org.junit.jupiter.api.*;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for Exercise4.
 */
@DisplayName("4-th exercise Test")
public class StringMakerTest {
    private final static PipedOutputStream pipe = new PipedOutputStream();
    private final static ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final static ByteArrayOutputStream err = new ByteArrayOutputStream();

    private final static InputStream originalIn = System.in;
    private final static PrintStream originalOut = System.out;
    private final static PrintStream originalErr = System.err;

    @BeforeAll
    public static void catchIO() throws IOException {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
        System.setIn(new PipedInputStream(pipe));
    }

    @AfterAll
    public static void releaseIO() {
        System.setOut(originalOut);
        System.setErr(originalErr);
        System.setIn(originalIn);
    }


    @Test
    @DisplayName("4-th exercise Test")
    public void StringMakerTestMain() throws Exception {

        StringMaker.main(new String[0]);
        String[] output = out.toString().split(System.lineSeparator());
        assertEquals("Step 1: This", output[0]);
        assertEquals("Step 2: This is", output[1]);
        assertEquals("Step 3: This is not a", output[2]);
        assertEquals("Result: This is not a string", output[3]);
    }
}