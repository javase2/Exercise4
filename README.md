# Exercise4
Отладка Java приложений

#Опция "public static void main(...)" для "StringMaker.java"

#начало копирования

public class StringMaker {
  
   public static void main(String... params) {
       
   }
}

#конец копирования

#Скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "StringMaker build",
            "command": "mvn clean install",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise4",
                "component": "maven"
            },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "StringMaker build and run",
            "command": "mvn clean install && java -jar ./target/*.jar",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise4",
                "component": "maven"
             },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "StringMaker test",
            "command": "mvn verify",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise4",
                "component": "maven"
             },
            "problemMatcher": []
        }
    ]
}
